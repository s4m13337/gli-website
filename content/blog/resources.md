---
title: "Resources"
date: 2022-01-10T20:48:59+05:30
---
This is a list of links that I keep and update regularly. It might be of use to you.

#### Learning Resources
- [https://learnxinyminutes.com/](https://learnxinyminutes.com/) |	what the name suggests
- [https://www.raspberrypi.org/forums/viewtopic.php?t=251645](https://www.raspberrypi.org/forums/viewtopic.php?t=251645) | guide to install KODI on rpi
- [https://shellscript.sh](https://shellscript.sh) | Shell scripting guide
- [https://tldp.org/LDP/abs/html/](https://tldp.org/LDP/abs/html/) | Shell scripting guide
- [http://www.eskimo.com/~scs/cclass/cclass.html](http://www.eskimo.com/~scs/cclass/cclass.html) | C Programming Guide
- [https://quadpoint.org/articles/irssi/](https://quadpoint.org/articles/irssi/) | Guide to using irssi
- [https://tldp.org/](https://tldp.org/) | The Linux Documentation project, lots of html style guides
- [https://www.cheatsheet.wtf/](https://www.cheatsheet.wtf/) | cheatsheet for Linux related stuff
- [https://github.com/marcotrosi/C](https://github.com/marcotrosi/C) | C programming practical kind of guide
- [https://keybr.com](https://keybr.com) | Typing practice
- [https://monkeytype.com](https://monkeytype.com) | same as above, but for improving speed imo
- [http://xahlee.info/](http://xahlee.info/) | Pretty cool website with LOTS of tutorials
- [http://linux.die.net/](http://linux.die.net/) | nice linux tutorials
- [https://explainshell.com/](https://explainshell.com/) | Explains any shell command you want, interactively! 
- [https://plainjs.com/](https://plainjs.com/) - How to do things in plainjs and ditch jQuery for good!
- [https://linuxjourney.com/](https://linuxjourney.com/) - Linux Journey, a nice resource for linux beginners.
- [https://www.gnu.org/software/bash/manual/bash.html](https://www.gnu.org/software/bash/manual/bash.html) | GNU bash manual
- [https://wiki.archlinux.org](https://wiki.archlinux.org) | Arch Wiki, contains useful non arch specific things

#### Other Stuff worth mentioning

- [https://distrotest.net/index.php](https://distrotest.net/index.php) | Test linux distros on the web!
- [https://www.redditdiscuss.com/](https://www.redditdiscuss.com/) | A place to find reddit discussions of episodes and TV series
- [http://68k.news/index.php?section=nation&loc=IN](http://68k.news/index.php?section=nation&loc=IN) | **Headlines from the future** HTML no bullshit news site
- [https://textfiles.com](https://textfiles.com) | **HUGE** Collection of text files
- [http://www.troubleshooters.com/linux/index.htm](http://www.troubleshooters.com/linux/index.htm) | a deep dive in linux, user software and hardware on linux enveironment, as experienced by steve litt
- [https://wiki.nikitavoloboev.xyz/operating-systems/linux/nixos](https://wiki.nikitavoloboev.xyz/operating-systems/linux/nixos) | want to learn more NixOS? start here with a curated list of resources by nikita voloboev.
- [https://nosystemd.org/](https://nosystemd.org/
) | Don't like systemd? here are the alternatives
- [https://www.tenouk.com/](https://www.tenouk.com/) | C and C++ "learning by doing" FOSS and comercial projects.

#### Extra, un-audited.
_These sites are yet to be reviewed our group memebers. open them and figure out what they are about. be adventurous_

https://ryanstutorials.net/linuxtutorial/
https://lym.readthedocs.io/en/latest/
https://www.billdietrich.me/Linux.html
https://magusz.neocities.org/
https://www.learnshell.org/
https://www.slackbook.org/html/index.html
https://www.etalabs.net/compare_libcs.html
https://iorgos.net/posts/ricing-illusion-productivity/
http://www.6502.org/documents/publications/dr_dobbs_journal/
https://digdeeper.neocities.org/

